window.addEventListener('DOMContentLoaded', function() {
  var masterExt = navigator.engmodeExtension || navigator.jrdExtension || navigator.kaiosExtension,
      niander = document.getElementById('niander'),
      sw = document.getElementById('switch'),
      appOrigin = 'omnibb.bananahackers.net',
      internalWebappPath = '/data/local/webapps/' + appOrigin + '/application.zip',
      internalStoragePath = '/sdcard',
      tempName = 'tmpbin.bin',
      internalPath = internalStoragePath + '/' + tempName,
      linearScript = [
        'mount -o remount,rw /',
        'sleep 0.5',
        'chmod -R 755 /sbin',
        'cp ' + internalPath + ' /sbin/busybox',
        'chown root:root /sbin/busybox && chmod -R 0755 /sbin && chmod 4755 /sbin/busybox',
        'sleep 0.5',
        'mount -o remount,ro /',
        'rm ' + internalPath
      ].join(' && ')
  
  function resourceLoader(resourcePath, done) {
    var req = new XMLHttpRequest(),
        segmentLength = 400;
    console.log('Fetching resource:', resourcePath)
    req.open('GET', resourcePath, true)
    req.responseType = 'blob'
    req.overrideMimeType('application/octet-stream')
    req.onload = function(e) {
      var buf = req.response
      console.log('Resource fetched:', resourcePath)
      if(buf) {
        var sdcard = navigator.getDeviceStorage("sdcard")
        var request = sdcard.addNamed(buf, tempName)
        request.onsuccess = function() {
          console.log('Buffer written')
	  done(true)
        }
        request.onerror = function(e) {
          window.alert('Resource write failed: ' + this.error.name)
          masterExt.startUniversalCommand('rm ' + internalPath, true)
          done(false)
        }
      }
      else {
        window.alert('Resource fetching failed: ' + resourcePath)
        done(false)
      }
    }
    req.send(null)
  }
  
  function installBB(done) {
    resourceLoader('/rsrc/busybox.bin', function(status) {
      if(status) {
        console.log('Busybox loaded')
        var executor = masterExt.startUniversalCommand(linearScript, true)
        executor.onsuccess = done
      }
    })
  }
  
  window.addEventListener('keydown', kdx = function(e) {
    if(e.key === 'Enter') {
      e.preventDefault()
      sw.querySelector('span').textContent = 'Running...'
      installBB(function() {
        sw.style.display = 'none'
        niander.classList.add('active')
        window.removeEventListener('keydown', kdx)
      })
    }
  })
  
})
